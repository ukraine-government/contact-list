// DOM
const blur = document.getElementById('blur');
const $newContact = document.getElementById('newContact');
const $addData = document.getElementById('new-data');
const $list = document.getElementById('list');
const $addWindow = document.getElementById('add');
const $searchValue = document.getElementById('input');
const $searchBtn = document.getElementById('searchBtn');
let b64 = document.getElementById("b64");

const $addBtn = document.getElementById('addBtn');
const $addPhoneBtn = document.getElementById('addPhoneBtn');
const $closeBtn = document.querySelectorAll('.closeBtn');
const $editBtn = document.querySelector('.fa-edit');
const $removeBtn = document.querySelector('.fa-trash-alt');

const $contactWindow = document.getElementById('contact-window');
const $contactWindowContainer = document.getElementById('contact-window-container');

const $nameField = document.getElementById('name');
const $surnameField = document.getElementById('surname');
const $birthdayField = document.getElementById('birthday');
const $mailField = document.getElementById('email');
const $phoneField = document.getElementById('phone-number');

// Render

let contactStorage = [];

render();

function render() {
    $list.innerHTML = '';
    if (localStorage.getItem('contactStorage')) {
        let render = JSON.parse(localStorage.getItem('contactStorage'));
        contactStorage = render;
        render.forEach(el => {
            const newLi = document.createElement('li');
            newLi.classList.add('contact');
            newLi.innerHTML = `<p id='id'>${el.id}</p>${el.name} ${el.surname}`;
            $list.append(newLi);
        });

    }
}

// Birthday Check

contactStorage.forEach(el => {
    let today = new Date();
    let dd = today.getDate();
    if (dd < 10) {
        dd = `0${dd}`;
    }
    let mm = today.getMonth() + 1;
    today = `${mm}-${dd}`;
    let birthday = JSON.stringify(el.birthday).slice(6, 11);
    if (birthday === today) {
        alert(`${el.name} ${el.surname} has a birthday today!`);
    }
});

// Functions

function clearBuffer() {
    b64.innerHTML = '';
}

function readFile() {

    if (this.files && this.files[0]) {

        const FR = new FileReader();

        FR.addEventListener("load", e => {
            document.getElementById("img").src = e.target.result;
            b64.innerHTML = e.target.result;
            console.log('works')
        });
        FR.readAsDataURL(this.files[0]);
    }

}

function editFile() {

    if (this.files && this.files[0]) {

        const FR = new FileReader();

        FR.addEventListener("load", e => {
            document.getElementById("edit-img").src = e.target.result;
            b64.innerHTML = e.target.result;
            console.log('works')
        });
        FR.readAsDataURL(this.files[0]);
    }

}

function search() {
    let search = contactStorage.filter(el => $searchValue.value === el.surname);
    if (search) {
        $list.innerHTML = '';
        $searchValue.value = '';
        search.forEach(el => {
            const newLi = document.createElement('li');
            newLi.classList.add('contact');
            newLi.innerHTML = `<p id='id'>${el.id}</p>${el.name} ${el.surname} <button id="reset" onclick="render()">Reset</button>`;
            $list.append(newLi);
        });
    } else {
        alert('Name is not found');
    }
}

function save() {
    localStorage.setItem('contactStorage', JSON.stringify(contactStorage));
}

function add() {
    if ($nameField.value === '') {
        alert('Write Name Please!');
    } else if ($surnameField.value === '') {
        alert('Write Surname Please!');
    } else {
        const newLi = document.createElement('li');
        let val;
        if (document.body.contains($list.firstElementChild)) {
            val = Number($list.lastElementChild.firstChild.textContent);
        } else {
            val = 0;
        }
        val++;
        let additionalPhones = [];
        document.querySelectorAll('#new-phone-number').forEach(el => {
            additionalPhones.push(el.value);
        });
        const newContact = {
            id: val,
            name: $nameField.value,
            surname: $surnameField.value,
            birthday: $birthdayField.value,
            mail: $mailField.value,
            phone: $phoneField.value,
            otherPhones: additionalPhones,
            photo: b64.textContent
        };
        newLi.classList.add('contact');
        newLi.innerHTML = `<p id="id">${val}</p>${$nameField.value} ${$surnameField.value}`;
        $list.append(newLi);
        contactStorage.push(newContact);
        save();
        $addData.reset();
        document.querySelectorAll('#new-phone-number').forEach(el => el.remove());
        document.querySelectorAll('#new-label').forEach(el => el.remove());
        clearBuffer();
    }
}

// Events

document.getElementById("photo").addEventListener("change", readFile);

$searchValue.addEventListener('keypress', e => {
    if (e.keyCode === 13) {
        search();
    }
});

document.body.addEventListener('click', e => {
    if (e.target === $searchBtn) {
        search();
    }
    if (e.target === $newContact) {
        $addWindow.classList.toggle('active');
        blur.classList.toggle('active');
    } else if (e.target === document.querySelector('ul li i')) {
        $addWindow.classList.toggle('active');
        blur.classList.toggle('active');
    }
    if (e.target === blur) {
        $addWindow.classList.remove('active');
        $contactWindowContainer.classList.remove('active-contact');
        blur.classList.remove('active');
    }
    if (e.target.classList.contains('contact')) {
        $contactWindowContainer.classList.toggle('active-contact');
        let additionals;
        let id = contactStorage.find(el => el.id === Number(e.target.firstChild.textContent));
        if (id.otherPhones.length) {
            additionals = id.otherPhones.join(',').replace(/,/g, '\n').split();
        } else {
            additionals = 'No Additional Phones';
        }
        $contactWindow.innerHTML = `<p id="id">${id.id}</p>
                                    <label for="img">Photo</label><img src="${id.photo}" width="60" height="auto" alt="" id="img">
                                    <label for="name">Name</label><p id="name">${id.name}</p>
                                    <label for="surname">Surname</label><p id="surname">${id.surname}</p>
                                    <label for="birthday">Birthday</label><p id="birthday">${id.birthday}</p>
                                    <label for="mail">E-Mail</label><p id="mail">${id.mail}</p>
                                    <label for="contact">Main Phone Number</label><p id="contact">${id.phone}</p>
                                    <label for="additional">Additional Phone Numbers</label><p id="additional">${additionals}</p>`;
        blur.classList.toggle('active');
    }
});

$addWindow.addEventListener('click', e => {
    e.stopPropagation();
});

$addWindow.addEventListener('keypress', e => {
    if (e.keyCode === 13) {
        add();
    }
});

$closeBtn.forEach(el => {
    el.addEventListener('click', () => {
        $addWindow.classList.remove('active');
        blur.classList.remove('active');
        $contactWindowContainer.classList.remove('active-contact');
    });
});

$addPhoneBtn.addEventListener('click', () => {
    const newInput = document.createElement('input');
    const newLabel = document.createElement('label');
    newLabel.textContent = 'Additional Phone';
    newLabel.id = 'new-label';
    newInput.id = 'new-phone-number';
    newInput.type = 'tel';
    newInput.placeholder = '123-456-7890';
    $addData.appendChild(newInput);
    $addData.insertBefore(newLabel, newInput);
});

$addBtn.addEventListener('click', () => {
    add();
});

$editBtn.addEventListener('click', e => {
    let id = contactStorage.find(el => el.id === Number(e.target.parentElement.childNodes[3].firstChild.textContent));
    let additionals = [];
    if (id.otherPhones.length) {
        additionals = id.otherPhones;
    } else {
        additionals = [];
    }
    $contactWindow.innerHTML = `<p id="id">${id.id}</p>
                                    <img id="edit-img" height="60" width="auto"><input type="file" id="edit-photo" accept=".jpg, .jpeg, .png">
                                    <label id="select-file" for="edit-photo">Edit Contact's Photo</label>
                                    <label for="photo">Photo</label><img src="${id.photo}" width="60" height="auto" alt="" id="img">
                                    <label for="name">Name</label><input id="name" type="text" value=${id.name}>
                                    <label for="surname">Surname</label><input id="surname" type="text" value=${id.surname}>
                                    <label for="birthday">Birthday</label><input id="birthday" type="date" value=${id.birthday}>
                                    <label for="mail">E-Mail</label><input id="mail" type="email" value=${id.mail}>
                                    <label for="contact">Main Phone Number</label><input id="contact" type="tel" placeholder="123-456-7890" value=${id.phone}>
                                    <span id="editPhoneBtn">Add Another Phone</span>
                                    <span id="saveBtn">Save</span>`;
    additionals.forEach(el => {
        if (el) {
            const editPhoneBtn = document.getElementById('editPhoneBtn');
            const newInput = document.createElement('input');
            const newLabel = document.createElement('label');
            newLabel.textContent = 'Additional Phone';
            newLabel.id = 'new-label';
            newInput.id = 'new-phone-number';
            newInput.type = 'tel';
            newInput.placeholder = '123-456-7890';
            newInput.value = el;
            $contactWindow.insertBefore(newLabel, editPhoneBtn);
            $contactWindow.insertBefore(newInput, editPhoneBtn);
        }
    });
    editPhoneBtn.addEventListener('click', () => {
        const newInput = document.createElement('input');
        const newLabel = document.createElement('label');
        newLabel.textContent = 'Additional Phone';
        newLabel.id = 'new-label';
        newInput.id = 'new-phone-number';
        newInput.type = 'tel';
        newInput.placeholder = '123-456-7890';
        $contactWindow.insertBefore(newLabel, editPhoneBtn);
        $contactWindow.insertBefore(newInput, editPhoneBtn);
    });
    document.getElementById("edit-photo").addEventListener("change", editFile); // photo edit
    document.querySelector('#saveBtn').addEventListener('click', e => {
        let photo;
        if (b64.textContent) {
            photo = b64.textContent;
        } else {
            photo = id.photo;
        }
        id.photo = photo;
        id.name = e.target.parentElement.childNodes[11].value;
        id.surname = e.target.parentElement.childNodes[14].value;
        id.birthday = e.target.parentElement.childNodes[17].value;
        id.mail = e.target.parentElement.childNodes[20].value;
        id.phone = e.target.parentElement.childNodes[23].value;
        id.otherPhones = [];
        document.querySelectorAll('#new-phone-number').forEach(el => {
            id.otherPhones.push(el.value);
        });
        if (id.otherPhones.length) {
            additionals = id.otherPhones.join(',').replace(/,/g, '\n').split();
        } else {
            additionals = 'No Additional Phones';
        }
        $contactWindow.innerHTML = `<p id="id">${id.id}</p>
                                    <label for="photo">Photo</label><img src="${id.photo}" width="60" height="auto" alt="" id="img">
                                    <label for="name">Name</label><p id="name">${id.name}</p>
                                    <label for="surname">Surname</label><p id="surname">${id.surname}</p>
                                    <label for="birthday">Birthday</label><p id="birthday">${id.birthday}</p>
                                    <label for="email">E-Mail</label><p id="email">${id.mail}</p>
                                    <label for="contact">Main Phone Number</label><p id="contact">${id.phone}</p>
                                    <label for="additional">Additional Phone Numbers</label><p id="additional">${additionals}</p>`;
        save();
        clearBuffer();
    });
});

$removeBtn.addEventListener('click', e => {
    let id = contactStorage.find(el => el.id === Number(e.target.parentElement.childNodes[3].firstChild.textContent));
    let index = contactStorage.indexOf(id);
    contactStorage.splice(index, 1);
    e.target.parentElement.remove();
    save();
    blur.classList.toggle('active');
    location.reload();
});